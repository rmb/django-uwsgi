django-uwsgi (1.1.2-2) UNRELEASED; urgency=medium

  * Team upload.
  * d/t/autopkgtest-pkg-python.conf: set import_name for autodep8
    (Closes: #997874) 
 -- Mohammed Bilal <mdbilal@disroot.org>  Sun, 17 Apr 2022 19:12:34 +0000

django-uwsgi (1.1.2-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Dominik George ]
  * New upstream version.
    + Switch upstream to django-uwsgi-ng.
      + Add README.source documenting the upstream switch.
    + Rename binary packages, and provide transitional packages.
    + Refresh patches.
  * Friendly takeover of Uploaders after "soft-orphan" by fladi.
  * Remove left-over .docs file for python-django-uwsgi.
  * Fix broken paths in doc-base file.

 -- Dominik George <natureshadow@debian.org>  Thu, 07 Oct 2021 15:35:12 +0200

django-uwsgi (0.2.2-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Thomas Goirand ]
  * Team upload.
  * Removed Python 2 support, as Django 2.2 doesn't support it anymore.

 -- Thomas Goirand <zigo@debian.org>  Mon, 22 Jul 2019 03:06:29 +0200

django-uwsgi (0.2.2-1) unstable; urgency=low

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout
  * Use 'python3 -m sphinx' instead of sphinx-build for building docs

  [ Michael Fladischer ]
  * New upstream release.
  * Add debian/gbp.conf.
  * Refresh patches.
  * Bump debhelper compatibility and version to 11.
  * Bump Standards-Version to 4.2.1.
  * Run wrap-and-sort -bast to reduce diff size of future changes.
  * Clean up django_uwsgi.egg-info/PKG-INFO to allow two builds in a
    row.
  * Enable autopkgtest-pkg-python testsuite.
  * Adapt doc-base registration to new doucmentation directory
    structure.

 -- Michael Fladischer <fladi@debian.org>  Fri, 31 Aug 2018 09:16:00 +0200

django-uwsgi (0.2.1-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 4.0.0.
  * Add patch to use local copies of screenshot images.

 -- Michael Fladischer <fladi@debian.org>  Mon, 17 Jul 2017 09:30:04 +0200

django-uwsgi (0.1.6-1) unstable; urgency=low

  * New upstream release.
  * Clean up .DS_Store files before install.

 -- Michael Fladischer <fladi@debian.org>  Wed, 19 Oct 2016 12:17:29 +0200

django-uwsgi (0.1.3-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ Michael Fladischer ]
  * Add python3-sphinx-rtd-theme to Build-Depends (Closes:
    830318,830326).
  * Clean django_uwsgi.egg-info/SOURCES.txt to allow two builds in a
    row.
  * Bump Standards-Version to 3.9.8.

 -- Michael Fladischer <fladi@debian.org>  Thu, 18 Aug 2016 21:37:45 +0200

django-uwsgi (0.1.3-2) unstable; urgency=medium

  * Fix name of doc-base registration (Closes: #802887).
  * git-dpm config.
  * Initialize git-dpm.

 -- Michael Fladischer <fladi@debian.org>  Mon, 26 Oct 2015 20:07:11 +0100

django-uwsgi (0.1.3-1) unstable; urgency=low

  * Initial release (Closes: #798987).

 -- Michael Fladischer <fladi@debian.org>  Tue, 28 Apr 2015 17:35:01 +0200
