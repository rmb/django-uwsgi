Screenshots
===========

`django-debug-toolbar <http://django-debug-toolbar.readthedocs.org/en/latest/>`_ panel

.. image:: screenshots/screenshot1.png



`Wagtail <https://github.com/torchbox/wagtail>`_ admin interface:

.. image:: https://edugit.org/AlekSIS/libs/django-uwsgi-ng/-/raw/master/docs/screenshots/screenshot2.png

.. image:: https://edugit.org/AlekSIS/libs/django-uwsgi-ng/-/raw/master/docs/screenshots/screenshot3.png

Emperor's Vassal Admin Panel

.. image:: https://edugit.org/AlekSIS/libs/django-uwsgi-ng/-/raw/master/docs/screenshots/screenshot4.png

`django.contrib.admin <https://docs.djangoproject.com/en/1.10/ref/contrib/admin/>`_ interface

.. image:: https://edugit.org/AlekSIS/libs/django-uwsgi-ng/-/raw/master/docs/screenshots/screenshot5.png
