django-uwsgi
~~~~~~~~~~~~

|release| |stats|  |lic|

.. |release| image:: https://img.shields.io/pypi/v/django-uwsgi-ng.svg
    :target: https://pypi.python.org/pypi/django-uwsgi-ng

.. |stats| image:: https://img.shields.io/pypi/dm/django-uwsgi-ng.svg
    :target: https://pypi.python.org/pypi/django-uwsgi-ng

.. |lic| image:: https://img.shields.io/pypi/l/django-uwsgi-ng.svg
    :target: https://pypi.python.org/pypi/django-uwsgi-ng


Django related examples/tricks/modules for uWSGI

Forked from: https://github.com/unbit/django-uwsgi


Screenshots
~~~~~~~~~~~

`django-debug-toolbar <http://django-debug-toolbar.readthedocs.org/en/latest/>`_ panel

.. image:: https://edugit.org/AlekSIS/libs/django-uwsgi-ng/-/raw/master/docs/screenshots/screenshot1.png


`Wagtail <https://github.com/torchbox/wagtail>`_ admin interface:

.. image:: https://edugit.org/AlekSIS/libs/django-uwsgi-ng/-/raw/master/docs/screenshots/screenshot2.png

.. image:: https://edugit.org/AlekSIS/libs/django-uwsgi-ng/-/raw/master/docs/screenshots/screenshot3.png

Emperor's Vassal Admin Panel

.. image:: https://edugit.org/AlekSIS/libs/django-uwsgi-ng/-/raw/master/docs/screenshots/screenshot4.png

`django.contrib.admin <https://docs.djangoproject.com/en/1.10/ref/contrib/admin/>`_ interface

.. image:: https://edugit.org/AlekSIS/libs/django-uwsgi-ng/-/raw/master/docs/screenshots/screenshot5.png

Documentation
~~~~~~~~~~~~~

`Read the documentation at rtfd.org <http://django-uwsgi-ng.rtfd.org/>`_

Contributors
~~~~~~~~~~~~

See `CONTRIBUTORS <https://edugit.org/AlekSIS/libs/django-uwsgi-ng/-/raw/master/CONTRIBUTORS>`_

License
~~~~~~~

`MIT <https://edugit.org/AlekSIS/libs/django-uwsgi-ng/-/raw/master/LICENSE>`_
